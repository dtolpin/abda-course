<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>ABDA: Approximate computation</title>

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/white.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/arduino-light.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
		<style type="text/css">
.reveal .slides section .static { visibility: hidden; height: 0; width: 0} /* replace with animated for PDF export */

.reveal .slides section .highlight-current-bg-cyan,
.reveal .slides section .highlight-current-bg-magenta,
.reveal .slides section .fragment.highlight-current-bg-yellow,
.reveal .slides section .highlight-bg-cyan,
.reveal .slides section .highlight-bg-magenta,
.reveal .slides section .fragment.highlight-bg-yellow {
	opacity: 1;
	visibility: inherit; }

		.reveal .slides section .highlight-bg-cyan.visible {
			background-color: cyan;
		}
		.reveal .slides section .highlight-bg-magenta.visible {
			background-color: magenta;
		}
		.reveal .slides section .fragment.highlight-bg-yellow.visible {
			background-color: yellow;
		}
		.reveal .slides section .highlight-current-bg-cyan.current-fragment {
			background-color: cyan;
		}
		.reveal .slides section .highlight-current-bg-magenta.current-fragment {
			background-color: magenta;
		}
		.reveal .slides section .fragment.highlight-current-bg-yellow.current-fragment {
			background-color: yellow;
		}

        .reveal .slides th {text-align: center}
		</style>
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h2>Applied Bayesian Data Analysis</h2>
							<h1>Approximate computation</h1>
					<h3>Concepts:</h3>
					<ul>
						<li>Quadrature</li>
						<li>Inverse CDF, rejection, importance sampling</li>
						<li>MCMC, MH, HMC, NUTS</li>
					</ul>
					<p><emph>David Tolpin, <a href="mailto:david.tolpin@gmail.com">david.tolpin@gmail.com</a></p>
				</section>

				<section>
					<section>
						<h2>Approximate Bayesian computation</h2>

						<ul>
							<li>We need:
								<ul>
									<li>posterior distribution $p(\theta|y)$</li>
									<li>posterior <i>predictive</i> distribution $p(\tilde y|y)$</li>
								</ul>
							</li>
							<li class="fragment">We use posterior to compute <i>integrals</i>:
								<ul>
									<li>mean</li>
									<li>variance</li>
									<li>condifence intervals</li>
									<li>expected utility</li>
								</ul>
							</li>
							<li class="fragment">
								We approximate the posterior with <i>samples</i>
							</li>
						</ul>
					</section>

					<section>
						<h2>Log scale</h2>
						<ul>
							<li>100 observations from unit normal:
										$\mathcal{N}(y_{1:100}|0, 1) \approx 1E-2000$
								<pre>
In [13]: from scipy.stats import norm
In [15]: norm.pdf(norm.rvs(100))
Out[15]: 0.0
								</pre>
							</li>
							<li>Log scale:
								$\log \mathcal{N}(y_{1:100}|0, 1) \approx -5000$
								<pre>
In [13]: from scipy.stats import norm
In [15]: norm.logpdf(norm.rvs(100))
Out[15]: -5121.5
								</pre>
							</li>
					</section>

					<section>
						<h2>Log scale: sum instead of product</h2>
						$$\log p(y_{1:N}) = \log \prod_{i=1}^N p(y_i) = \sum_{i=1}^N \log p(y_i)$$
<pre><code class="go" data-trim>
func (m *Model) Observe(x []float64) float64 {
  ll := Normal.Logps(0, 1, x...)
  for i := range m.Data {
  	ll += Normal.Logp(x[0], math.Exp(x[1]),
  		m.Data[i])
  }
  return ll
}
</code><pre>
					</section>

					<section>
						<h2>Log scale: LogSumExp</h2>
						$$p(x) = p(x,y) + p(x,\neg y)$$
						$$\log p(x) = \log \left( \exp(\log p(x,y)) + \exp(\log p(x, \neg y))\right)$$
<pre><code class="go" data-trim>
func LogSumExp(x, y float64) float64 {
	max := x
	if y > max {
		max = y
	}
	return max + math.Log(math.Exp(x-max)+math.Exp(y-max))
}
</code></pre>
						<p>Example: <a href="https://bitbucket.org/dtolpin/infergo/src/master/examples/gmm/model/model.go">Gaussian Mixture Model</a></p>
					</section>

					<section>
						<h2>Numerical integration — Quadrature</h2>
						<ul>
							<li>
								Monte Carlo approximation:
								$$E(h(\theta)|y) = \int h(\theta)p(\theta|y)d(y)d\theta \approx \frac 1 S \sum_{s=1}^S h(\theta^s)$$
							</li>
							<li>
								Low of large numbers for <b>independent samples</b>:
								$$\lim_{S\to \infty} \frac 1 S \sum_{s=1}^S h(\theta^S) = E(h(\theta)|y)$$
							</li>
						</ul>
					</section>
				</section>

				<section>
					<section>
						<h2>Monte Carlo methods</h2>
						<ul>
							<li>Inverse transform sampling</li>
							<li>Rejection sampling</li>
							<li>Importance sampling</li>
						</ul>
					</section>
					<section>
						<h2>Inverse transform sampling</h2>
						<ul>
							<li>Given CDF $F(x)$ (and inverse CDF $F^{-1}(x))</li>
							<li>Sample $U$ from $[0, 1]$ uniformly</li>
							<li>Compute $v = F^{-1}(U)$.</li>
						</ul>
						<p>(<a href="https://en.wikipedia.org/wiki/Inverse_transform_sampling#Proof_of_correctness">Proof of correctness</a>)</p>
					</section>
					<section>
						<h2>Rejection sampling</h2>
						<ul>
							<li>Target $q(\theta|y)$, proposal $g(\theta)$</li>
							<li>Draw $v$ from $g(\theta)$</li>
							<li>Draw $u$ uniformly from $[0, 1]$</li>
							<li>Accept if $\frac {q(\theta|y)} {Mg(\theta)} > u$</li>
						</ul>
						<p>
							<img src="rejection.png" width="60%"/>
						</p>
					</section>
					<section>
						<h2>Rejection sampling</h2>
						<table>
							<tr>
								<th>general</th>
								<th>truncated</th>
							</tr>
							<tr>
								<td><img src="rejection-general.png"/></td>
								<td><img src="rejection-truncated.png"/></td>
							</tr>
						</table>
					</section>
					<section>
						<h2>Importance Sampling</h2>
						<ul>
							<li>Target $q(\theta|y)$, proposal $g(\theta)$</li>
							<li>Draw $v$ from $g(\theta)$</li>
							<li>$E(h(\theta|y)) = \frac {\sum_{i=1}^S w_s h(\theta^s)}  {\sum_{i=1}^S w_s}$ where $w_s = \frac {q(\theta^s)} {g(\theta^s)}$</li>
							<li>For some $\theta$, $q(\theta)$ can be higher than $g(\theta)$</li>
						</ul>
					</section>
				</section>

				<section>
					<section>
						<h2>Markov Chain Monte Carlo</h2>
						<ul>
							<li>Draws samples from Markov chain</li>
							<li>Easy to sample</li>
							<li>Chain equilibrium samples from posterior</li>
							<li>(&#x1f44e;) samples are dependent</li>
							<li>(&#x1f44e;) efficient chains can be hard</li>
						</ul>
					</section>
					<section>
						<h2>Markov Chain Monte Carlo</h2>
						<ul>
							<li>Markov chain</li>
							<li>Metropolis Hastings</li>
							<li>Gibbs</li>
						</ul>
					</section>
					<section>
						<h2>Markov chain</h2>
						<ul>
							<li> $\theta^1,\theta^2,\ldots$; $\theta^t$ depends only on $\theta^{(t-1)}$</li>
							<li> Chain starts with <b>starting point</b> $\theta^0$</li>
							<li> Transition distribution $T_t(\theta^t|\theta^{t-1})$ (may
								  depend on $t$)</li>
	<li> By choosing a suitable <i>transition distribution</i>, the <b>stationary distribution</b> of Markov chain is $p(\theta|y)$</li>
						</ul>
					</section>
					<section>
						<h2>Markov chain</h2>
						<ul>
							<li>Transition: $\theta^{t+1}|\theta^{t} \sim \mathcal{N}(0.5x^{t}, 1.0)$</li>
							<li>Posterior: $\theta \sim \mathcal{N}(0, 1.33)$</li>
						</ul>
						<p>
						<img src="markov-chain.png" style="width: 60%"/>
						</p>
					</section>
					<section style="font-size: smaller">
						<h2>Metropolis Hastings</h2> 
						<ul>
							<li>Initialize:
								<ul>
									<li>Pick $x_0$</li>
									<li>Set $t = 0$</li>
								</ul>
							</li>
							<li>Iterate:
								<ul>
									<li> Generate candidate $x'$ according to $g(x' \mid x_t)$.</li>
									<li> Calculate the acceptance probability $A(x', x_t) = \min\left(1, \frac{P(x')}{P(x_t)} \frac{g(x_t \mid x')}{g(x' \mid x_t)}\right)$</li>
									<li> Accept or reject:
										<ul>
											<li> generate a uniform random number $u \in [0, 1]$</li>
											<li> if $u \le A(x' , x_t)$, then accept: $x_{t+1} = x'$;</li>
											<li> if $u >   A(x' , x_t)$, then reject:  $x_{t+1} = x_{t}$.</li>
										</ul>
									</li>
									<li> Increment: $t = t + 1$.</li>
								</ul>
							</li>
						</ul>
					</section>
					<section>
						<h2>Metropolis-Hastings</h2>
						<p>
						<img src="3dRosenbrock.png" width="80%"/>
						</p>
				</section>

				<section>
					<section>
						<h2>Advanced Markov Chain Monte Carlo</h2>
						<ul>
							<li><a href="https://matt-graham.github.io/slides/hmc-tutorial/#/">HMC Tutorial</a></li>
							<li><a href="https://chi-feng.github.io/mcmc-demo/app.html?algorithm=RandomWalkMH&target=banana">Interactive demonstration</a></li>
						</ul>
					</section>
				</section>
			</div>
		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>
			// More info about config & dependencies:
			// * https://github.com/hakimel/reveal.js#configuration
			// * https://github.com/hakimel/reveal.js#dependencies
			Reveal.initialize({
				transition: 'fade',
				math: {
					mathjax: 'MathJax/MathJax.js',
					config: 'TeX-AMS_HTML-full'
				},
				dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
					{ src: 'plugin/math/math.js', async: true }
				]
			});
		</script>
	</body>
</html>

package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"math"
)

type Entry struct {
	Company string
	Count int
	Average float64
}

type Model struct {
	Data []Entry
}

func (m *Model) Observe(x []float64) float64 {
	logp := 0.

	// Parameters
	// Expected arrival time of any company
	logMu := x[0]
	// Variability between companies
	logTau := x[1]; tau := math.Exp(logTau)
	// Expected arrival times of each company
	logEta := x[2:]; eta := make([]float64, len(m.Data))
	for i := range eta {
		eta[i] = math.Exp(logEta[i])
	}
 
	// Prior
	// Either mu or tau must be constrained
	logp += Cauchy.Logp(0, 5, logTau)
	logp += Normal.Logps(logMu, tau, logEta...)

	// Conditional
	for i := range m.Data {
		// We assume that a single arrival time is
		// exponentially distributed. For an average
		// arrival time we know the shape (Count) and
		// guess the mean (eta).
		alpha := float64(m.Data[i].Count)
		beta := alpha / eta[i]
		logp += Gamma.Logp(alpha, beta, m.Data[i].Average)
	}
	return logp
}

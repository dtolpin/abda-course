package main

import (
	. "bitbucket.org/dtolpin/abda-course/examples/rides/model/ad"
	"bitbucket.org/dtolpin/infergo/infer"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Command line arguments

var (
	NSTEPS = 5
	STEP   = 0.1
	NITER  = 10000
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		log.Printf(`Inferring expected arrival times of taxis:
		rides [OPTIONS] < data.csv > results.csv` + "\n")
		flag.PrintDefaults()
	}
	flag.IntVar(&NSTEPS, "nsteps", NSTEPS, "number of leapfrog steps")
	flag.Float64Var(&STEP, "step", STEP, "leapfrog step size")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	log.SetFlags(0)
}

func main() {
	flag.Parse()

	if flag.NArg() != 0 {
		log.Fatalf("unexpected positional arguments: %v",
			flag.Args())
	}

	// Get the data
	var data []Entry
	entry := Entry{}
	rdr := csv.NewReader(os.Stdin)
	// Skip the header
	_, err := rdr.Read()
	if err != nil {
		log.Fatalf("invalid data: %v", err)
	}
	for {
		rec, err := rdr.Read()
		if err == io.EOF {
			break
		}
		entry.Company = rec[0]
		entry.Count, err = strconv.Atoi(rec[1])
		if err != nil {
			log.Fatalf("invalid data: %v", err)
		}
		entry.Average, err = strconv.ParseFloat(rec[2], 64)
		if err != nil {
			log.Fatalf("invalid data: %v", err)
		}
		data = append(data, entry)
	}
	log.Printf("Data: %v", data)

	// Create the model
	m := &Model{
		Data: data,
	}

	// Initialize the parameter vector
	x := make([]float64, 2 + len(m.Data))
	for i := range x {
		x[i] = 0.1 * rand.NormFloat64()
	}

	m.Observe(x)

	// Infer the posterior
	fmt.Printf("mu,tau")
	for i := range m.Data {
		fmt.Printf(",eta[%s]", m.Data[i].Company)
	}
	fmt.Println()

	hmc := &infer.HMC{
		L: NSTEPS,
		Eps: STEP / math.Sqrt(float64(len(m.Data))),
	}
	samples := make(chan []float64)
	hmc.Sample(m, x, samples)
	// Burn
	for i := 0; i != NITER; i++ {
		<-samples
	}

	// Collect after burn-in
	mu := 0.
	tau := 0.
	eta := make([]float64, len(m.Data))
	n := 0.
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		muS := math.Exp(x[0])
		fmt.Printf("%.3f", muS)
		mu += muS
		tauS := math.Exp(x[1])
		fmt.Printf(",%.3f", tauS)
		tau += tauS
		for i := 0; i != len(m.Data); i++ {
			etaS := math.Exp(x[i+2])
			fmt.Printf(",%.3f", etaS)
			eta[i] += etaS
		}
		fmt.Println()
		n++
	}
	hmc.Stop()

	// Output mean estimates of expected times
	mu /= n
	tau /= n
	for i := range eta {
		eta[i] /= n
	}
	log.Printf("Mean: mu=%.3f, tau=%.3f, eta=%.3f\n", mu, tau, eta)
}

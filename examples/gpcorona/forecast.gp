data = "forecast.dat"

if (ARG1 ne "")
	data = ARG1

set ylabel "y"
set xlabel "x"
plot data using 1:3:4 with filledcurves \
		  lc "#dddddd" title "68%", \
	 "" using 1:3:5 with filledcurves \
	      lc "#dddddd" title "", \
	 "" using 1:3 with lines lw 2 lc black title "predicted", \
	 "" using 1:2 with points pt 7 ps 0.5 lc "#cc6633" title "observed"

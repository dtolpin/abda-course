package priors

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"math"
)

type ARPriors struct{}

func (*ARPriors) Observe(x []float64) float64 {
	const (
		c = iota // variance
		l        // length scale
		s        // noise
	)

	ll := 0.

	// The data and the time should be standardized.
	ll += Normal.Logp(-1, 1, x[c]) // variance < 1
	ll += Normal.Logp(0, 1, x[l])  // length ≈ 1
	ll += Normal.Logp(-2, 1, x[s]) // noise ≈ 0.1

	return ll
}

type ARSPriors struct{}

func (*ARSPriors) Observe(x []float64) float64 {
	const (
		c1 = iota // trend scale
		c2        // season scale
		l1        // trend length scale
		l2        // season length scale
		p         // season period
		s         // noise
	)

	ll := 0.

	// trend weight is a number between 0 and 1
	ll += Normal.Logp(-1, 1, x[c1])
	// seasonality weight is normally lower than trend weight
	ll += Normal.Logp(x[c1]-math.Log(2), 1, x[c2])

	// length scale is around 1, in wide margins
	ll += Normal.Logp(0, 1, x[l1])
	// season volatility scale is roughly the period
	ll += Normal.Logp(x[p], 1, x[l2])

	// The period seems to be somewhere around 80.
	ll += Normal.Logp(4.5, 0.5, x[p])

	// The noise is around 0.1
	ll += Normal.Logp(-2, 1, x[s])

	return ll
}

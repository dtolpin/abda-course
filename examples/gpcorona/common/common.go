// Code common to different variants of the filter
package common

import (
	. "bitbucket.org/dtolpin/abda-course/examples/gpcorona/priors/ad"
	"bitbucket.org/dtolpin/gogp/gp"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// Command-line options
var (
	SEASONAL = false
	LOG      = false
	MEAN     = false
	NITER    = 100
	NPLATEAU = 10
	EPS      = 0.01
	RATE     = 0.5
	STEP     = 1.
	NSTEPS   = 30
	QUIET    = false
	PARALLEL = false
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			`Forecast in time series:
	%s [OPTIONS] < series.csv > forecast.csv
Options:
`, filepath.Base(os.Args[0]))
		flag.PrintDefaults()
	}
	flag.BoolVar(&SEASONAL, "seasonal", SEASONAL, "assume seasonality")
	flag.BoolVar(&LOG, "log", LOG, "log transform the data")
	flag.BoolVar(&MEAN, "mean", MEAN, "predict mean rather than median")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.Float64Var(&RATE, "rate", RATE, "learning rate")
	flag.Float64Var(&EPS, "eps", EPS, "optimization precision, in log-odds")
	flag.IntVar(&NPLATEAU, "nplateau", NPLATEAU, "number of plateau iterations")
	flag.Float64Var(&STEP, "step", STEP, "time step size for forecasting")
	flag.IntVar(&NSTEPS, "nsteps", NSTEPS, "number of time steps to forecast")
	flag.BoolVar(&QUIET, "quiet", QUIET, "suppress information output")
	flag.BoolVar(&PARALLEL, "parallel", PARALLEL, "compute covariances in parallel")
}

// Model cobmining GP and priors on hyperparameters
type Model struct {
	GP           *gp.GP
	Priors       model.Model
	GGrad, PGrad []float64
}

func (m *Model) Observe(x []float64) float64 {
	var gll, pll float64
	gll, m.GGrad = m.GP.Observe(x), model.Gradient(m.GP)
	pll, m.PGrad = m.Priors.Observe(x), model.Gradient(m.Priors)
	return gll + pll
}

func (m *Model) Gradient() []float64 {
	for i := range m.PGrad {
		m.GGrad[i] += m.PGrad[i]
	}

	return m.GGrad
}

// Read writes data from CSV.
func Read(rdr *csv.Reader) (
	header []string,
	X [][]float64,
	Y []float64,
) {
	header, err := rdr.Read() // skip the header
	if err != nil {
		log.Fatalf("cannot read data: %v", err)
	}
	for irow := 1; ; irow++ {
		row, err := rdr.Read()
		if err == io.EOF {
			break
		}

		xy := []float64{}
		for icell := range row {
			cell, err := strconv.ParseFloat(row[icell], 64)
			if err != nil {
				log.Fatalf("cell (%d, %d): invalid data %q: %v",
					irow+1, icell+1, row[icell], err)
			}
			xy = append(xy, cell)
		}
		if LOG && xy[1] <= 0 {
			// if log scale is on, skip zero outputs
			log.Printf("WARNING: row %d: non-positive %s with -log: %v",
				irow, header[1], xy[1])
			continue
		}
		X = append(X, xy[:1])
		Y = append(Y, xy[1])
	}

	return header, X, Y
}

// Fit fits GP hyperparameters.
func Fit(gp *gp.GP) {
	m := &Model{
		GP: gp,
	}
	if SEASONAL {
		m.Priors = &ARSPriors{}
	} else {
		m.Priors = &ARPriors{}
	}
	theta := make([]float64, gp.Simil.NTheta()+gp.Noise.NTheta())
	for i := range theta {
		theta[i] = 0.1 * rand.NormFloat64()
	}

	opt := &infer.Adam{Rate: RATE}
	ll0, _ := opt.Step(m, theta)
	ll, llprev := ll0, ll0
	iter := 0
	plateau := 0
	// Evolve x_, keep x_ with the highest log-likelihood in x
	theta_ := make([]float64, len(theta))
	copy(theta_, theta)
	for ; iter != NITER; iter++ {
		ll_, _ := opt.Step(m, theta_)
		// Store theta_ in theta if log-likelihood increased
		if ll_ > ll {
			copy(theta, theta_)
			ll = ll_
		}
		// Stop early if the optimization is stuck
		if ll_-llprev <= EPS {
			plateau++
			if plateau == NPLATEAU {
				break
			}
		} else {
			plateau = 0
		}
		llprev = ll_
	}
	if !QUIET {
		log.Printf("GP fitting: %d iterations, %f -> %f", iter+1, ll0, ll)
	}
}

// Write writes prediction results to CSV.
func Write(
	wtr *csv.Writer,
	header []string,
	X [][]float64, Y []float64,
	mu, sigma []float64,
	shift, scale float64,
) {
	header = append(header,
		"mean", "lower", "upper",
		"mu", "sigma", "log", "shift", "scale")
	wtr.Write(header)
	for i := range X {

		mean := mu[i]*scale + shift
		stddev := sigma[i] * scale
		lower := mean - stddev
		upper := mean + stddev
		if LOG {
			// median seems to work better than mean
			median := math.Exp(mean)
			lower = math.Exp(lower)
			upper = math.Exp(upper)
			if MEAN {
				mean = median * math.Exp(stddev*stddev/2)
				if mean > median {
					upper += mean - median
				} else {
					lower -= median - mean
				}
			} else {
				mean = median
			}
		}

		record := []string{
			strconv.FormatFloat(X[i][0], 'f', -1, 64),
			strconv.FormatFloat(Y[i], 'G', -1, 64),
		}
		for j := 1; j != len(X[0]); j++ {
			record = append(record,
				strconv.FormatFloat(X[i][j], 'G', -1, 64))
		}
		record = append(record,
			strconv.FormatFloat(mean, 'G', -1, 64))
		record = append(record,
			strconv.FormatFloat(lower, 'G', -1, 64))
		record = append(record,
			strconv.FormatFloat(upper, 'G', -1, 64))
		record = append(record,
			strconv.FormatFloat(mu[i], 'f', -1, 64))
		record = append(record,
			strconv.FormatFloat(sigma[i], 'f', -1, 64))
		record = append(record,
			strconv.FormatBool(LOG))
		record = append(record,
			strconv.FormatFloat(shift, 'G', -1, 64))
		record = append(record,
			strconv.FormatFloat(scale, 'G', -1, 64))
		wtr.Write(record)
	}
	wtr.Flush()
}

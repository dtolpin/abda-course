package common

import (
	"encoding/csv"
	"reflect"
	"strings"
	"testing"
)

var (
	data = `timestamp,volume
10,10
20,100
35,15
50,33
`
)

func TestRead(t *testing.T) {
	for i, c := range []struct {
		data   string
		header []string
		X      [][]float64
		Y      []float64
		LOG    bool
	}{
		{
			"timestamp,\"volume\"\n10,10\n35,15.0\n",
			[]string{"timestamp", "volume"},
			[][]float64{{10}, {35}},
			[]float64{10, 15},
			false,
		},
		{
			"timestamp,\"volume\"\n10,10\n0,15\n",
			[]string{"timestamp", "volume"},
			[][]float64{{10}, {0}},
			[]float64{10, 15},
			false,
		},
		{
			"timestamp,\"volume\"\n10,10\n35,0\n",
			[]string{"timestamp", "volume"},
			[][]float64{{10}},
			[]float64{10},
			true,
		},
	} {
		LOG = c.LOG
		header, X, Y := Read(
			csv.NewReader(strings.NewReader(c.data)))
		if !reflect.DeepEqual(header, c.header) {
			t.Errorf("%d: wrong header: got %q, want %q", i, header, c.header)
		}
		if !reflect.DeepEqual(X, c.X) {
			t.Errorf("%d: wrong X: got %f, want %f", i, X, c.X)
		}
		if !reflect.DeepEqual(Y, c.Y) {
			t.Errorf("%d: wrong Y: got %f, want %f", i, Y, c.Y)
		}
	}
}

func TestWrite(t *testing.T) {
	for i, c := range []struct {
		header       []string
		X            [][]float64
		Y            []float64
		mu, sigma    []float64
		log          bool
		shift, scale float64
		result       string
	}{
		{
			[]string{"timestamp", "volume"},
			[][]float64{{10}, {35}},
			[]float64{10, 15},
			[]float64{1, 3}, []float64{2, 3},
			false,
			10, 2,
			"timestamp,volume,mean,lower,upper,mu,sigma,log,shift,scale\n" +
				"10,10,12,8,16,1,2,false,10,2\n35,15,16,10,22,3,3,false,10,2\n",
		},
	} {
		var builder strings.Builder
		LOG = c.log
		Write(csv.NewWriter(&builder),
			c.header, c.X, c.Y,
			c.mu, c.sigma,
			c.shift, c.scale)
		result := builder.String()
		if result != c.result {
			t.Errorf("%d: wrong result: got\n%v\nwant\n%v", i, result, c.result)
		}
	}
}

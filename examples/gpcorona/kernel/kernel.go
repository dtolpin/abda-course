package kernel

import (
	"bitbucket.org/dtolpin/gogp/kernel"
)

// The autoregressive kernel; NExtra is the number of extra
// parameters (for use with noise); 0 by default.
type AR struct {
}

func (k AR) Observe(x []float64) float64 {
	const (
		c = iota // variance
		l        // length scale
		a        // first point
		b        // second point
	)

	return x[c] * x[c] * kernel.Matern52.Cov(x[l], x[a], x[b])
}

func (AR) NTheta() int { return 2 }

// The Autoregressive + Seasonal kernel.
type ARS struct{}

func (ARS) Observe(x []float64) float64 {
	const (
		c1 = iota // trend scale
		c2        // season scale
		l1        // trend length scale
		l2        // season length scale
		p         // season period
		xa        // first point
		xb        // second point
	)

	return x[c1]*x[c1]*kernel.Matern52.Cov(x[l1], x[xa], x[xb]) +
		x[c2]*x[c2]*kernel.Normal.Cov(x[l2], x[xa], x[xb])*
			kernel.Periodic.Cov(x[l1], x[p], x[xa], x[xb])
}

func (ARS) NTheta() int { return 5 }

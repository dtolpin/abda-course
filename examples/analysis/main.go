package main

import (
	. "bitbucket.org/dtolpin/abda-course/examples/analysis/model/ad"
	"bitbucket.org/dtolpin/infergo/ad"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Command line arguments

var (
	MODEL  = "Normal"
	RATE   = 0.1
	DECAY  = 0.998
	GAMMA  = 0.9
	STEP   = 0.1
	NITER  = 1000
	DARATE = 0.01
	NBURN  = 0
	NADPT  = 10
	DEPTH  = 3.
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		log.Printf(`Comparing models:
		analysis [OPTIONS] < data.csv > results.csv` + "\n")
		flag.PrintDefaults()
	}
	flag.StringVar(&MODEL, "model", MODEL,
		"model (Normal, LogNormal, HierLogNormal)")
	flag.Float64Var(&RATE, "rate", RATE, "learning rate")
	flag.Float64Var(&DECAY, "decay", DECAY, "rate decay")
	flag.Float64Var(&GAMMA, "gamma", GAMMA, "momentum factor")
	flag.Float64Var(&STEP, "step", STEP, "leapfrog step size")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.Float64Var(&DARATE, "darate", DARATE, "adaptation rate")
	flag.IntVar(&NBURN, "nburn", NBURN, "number of burned iterations")
	flag.IntVar(&NADPT, "nadpt", NADPT,
		"number of steps per adaptation")
	flag.Float64Var(&DEPTH, "depth", DEPTH, "target NUTS tree depth")
	log.SetFlags(0)
	ad.MTSafeOn() // to compute logp(x) in the inference loop
}

func main() {
	flag.Parse()
	if NBURN == 0 {
		NBURN = NITER
	}

	if flag.NArg() != 0 {
		log.Fatalf("unexpected positional arguments: %v",
			flag.Args())
	}

	// Get the data
	var data []float64
	rdr := csv.NewReader(os.Stdin)
	for {
		record, err := rdr.Read()
		if err == io.EOF {
			break
		}
		value, err := strconv.ParseFloat(record[0], 64)
		if err != nil {
			log.Fatalf("invalid data: %v", err)
		}
		data = append(data, value)
	}

	// Create the model
	var m model.Model
	switch MODEL {
	case "Normal":
		m = (*NormalModel)(&Model{Data: data})
	case "LogNormal":
		m = (*LogNormalModel)(&Model{Data: data})
	case "HierLogNormal":
		m = (*HierLogNormalModel)(&Model{Data: data})
	default:
		log.Fatalf("Unrecognized model %q", MODEL)
	}

	switch MODEL {
	case "HierLogNormal":
		fmt.Printf("estimate,mu,sigma,eta,logp\n")
	default:
		fmt.Printf("estimate,mu,sigma,logp\n")
	}
	var mu, sigma, eta float64

	// Estimate parameters from data, for comparison
	s := 0.
	s2 := 0.
	for i := 0; i != len(data); i++ {
		x := data[i]
		s += x
		s2 += x * x
	}
	mean := s / float64(len(data))
	stddev := math.Sqrt(s2/float64(len(data)) - mean*mean)
	switch MODEL {
	case "Normal":
		mu = mean
		sigma = stddev
	case "LogNormal", "HierLogNormal":
		cov := stddev / mean
		sigma = math.Sqrt(math.Log(cov*cov + 1))
		mu = math.Log(mean) - 0.5*sigma*sigma
	}
	x := []float64{mu, math.Log(sigma), 0.}
	ll := m.Observe(x)
	model.DropGradient(m)
	printTheta("data", x, ll)

	// Guess initial point
	for i := range x {
		x[i] = 0.5 * rand.NormFloat64()
	}
	ll = m.Observe(x)
	printTheta("guess", x, ll)

	// Estimate the maximum a posteriori
	opt := &infer.Momentum{
		Rate:  RATE / float64(len(data)),
		Decay: DECAY,
		Gamma: GAMMA,
	}
	for iter := 0; iter != NITER; iter++ {
		opt.Step(m, x)
	}
	if MODEL != "HierLogNormal" {
		x[2] = 0.
	}

	ll = m.Observe(x)
	model.DropGradient(m)
	printTheta("map", x, ll)

	// Infer the posterior
	nuts := &infer.NUTS{
		Eps: STEP / math.Sqrt(float64(len(data))),
	}
	samples := make(chan []float64)
	nuts.Sample(m, x, samples)
	// Adapt toward optimum tree depth.
	da := &infer.DepthAdapter{
		DualAveraging: infer.DualAveraging{Rate: RATE},
		Depth:         DEPTH,
		NAdpt:         NADPT,
	}
	da.Adapt(nuts, samples, NBURN)

	// Collect after burn-in
	mu, sigma, eta = 0., 0., 0.
	n := 0.
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		ll = m.Observe(x)
		model.DropGradient(m)
		printTheta("sample", x, ll)
		mu += x[0]
		sigma += math.Exp(x[1])
		eta += x[2]
		n++
	}
	nuts.Stop()
	mu /= n
	sigma /= n
	eta /= n

	x[0], x[1], x[2] = mu, math.Log(sigma), eta
	ll = m.Observe(x)
	model.DropGradient(m)
	printTheta("mean", x, ll)
}

func printTheta(est string, x []float64, ll float64) {
	switch MODEL {
	case "HierLogNormal":
		fmt.Printf("%s,%.6g,%.6g,%.6g,%.6g\n",
			est, x[0], math.Exp(x[1]), x[2], ll)
	default:
		fmt.Printf("%s,%.6g,%.6g,%.6g\n",
			est, x[0], math.Exp(x[1]), ll)
	}
}

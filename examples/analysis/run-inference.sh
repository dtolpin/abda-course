#!/bin/bash

for model in Normal LogNormal HierLogNormal; do
	./analysis -model $model < data.csv > results-$model.csv
done

// Inferring parameters of the Normal distribution from
// observations
package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"math"
)

type Model struct {
	Data []float64
}

// Observations are modelled by normal distribution
type NormalModel Model

func (m *NormalModel) Observe(x []float64) float64 {
	ll := Normal.Logps(0, 10, x...)
	mu := x[0]
	sigma := math.Exp(x[1])
	for i := range m.Data {
		ll += Normal.Logp(mu, sigma, m.Data[i])
	}
	return ll
}

// Observations are modelled by log-normal distribution
type LogNormalModel Model

func (m *LogNormalModel) Observe(x []float64) float64 {
	ll := Normal.Logps(0, 10, x...)
	mu := x[0]
	sigma := math.Exp(x[1])
	for i := range m.Data {
		ll += Normal.Logp(mu, sigma, math.Log(m.Data[i]))
	}
	return ll
}

// Log normal model with hierarchical prior on the mean
type HierLogNormalModel Model

// This particular model should give the same results as the
// non-hierarchical model, but this is of course not always
// the case in general
func (m *HierLogNormalModel) Observe(x []float64) float64 {
	ll := 0.
	mu := x[0]
	logSigma := x[1]
	eta := x[2]
	ll += Normal.Logp(0, 9.9, eta)
	ll += Normal.Logp(eta, 1, mu)
	ll += Normal.Logp(0, 10, logSigma)
	sigma := math.Exp(logSigma)
	for i := range m.Data {
		ll += Normal.Logp(mu, sigma, math.Log(m.Data[i]))
	}
	return ll
}
